# README #

This is a computer videogame for #gbjam3 summer 2014.
Help at our hero to beat an ancient record that dishonored your father... KALANDRAKA is laughing at you.

### Legend of Kalandraka repository ###

* Here is all source of Legend of Kalandraka, feel free to modify and improve!
* Version: Very very beta

### Setting up ###

* You need SFML 2.1
* A c++11 standard compatible C++ compiler: g++ 4.9 or higher
* make tool
* Compiling: make
* Launching: some rule of make: make test?

### Repository owner ###

* José Crespo Guerrero
* Participating at 2014 summer #gbjam3
* This is totally free software, feel free to modify or improve anything!