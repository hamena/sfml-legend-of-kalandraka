#ifndef CAPTION_HPP
#define CAPTION_HPP

#include <SFML/Graphics.hpp>
#include "button_manager.hpp"

using sf::Clock;

class CaptionManager{
public:
  CaptionManager(ButtonManager& bm) : 
    _lefthand(NONE), _righthand(NONE), _bmanager(bm), _left(false), _right(false) {}
  
  void captureLeftHand();
  void captureRightHand();
  void respond();
private:
  unsigned _lefthand, _righthand;
  ButtonManager& _bmanager;
  bool _left, _right;
  
};

#endif // CAPTION_HPP
