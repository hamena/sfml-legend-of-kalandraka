#include <SFML/Graphics.hpp>
#include <vector>
#include <string>
#include <iostream>
#include "texture_manager.hpp"

using std::vector;
using std::string;
using namespace sf;

struct Sentence{
  string _str;
  float _duration;
  Sentence(const String& s, float d) : _str(s), _duration(d) {}
};

struct Scene{
  Sprite _backgrd;
  vector<Sentence> _text;
  Scene(const Sprite& s, const vector<Sentence>& v) : _backgrd(s), _text(v){}
};

class History{
public:
  History(RenderWindow& w, TextureManager&);
  void play();
private:
  RenderWindow& _window;
  vector<Scene> _scenes;
  TextureManager& _tman;
  Font _font;
  Text _txt, _escape;
  Sprite _tvscreen;
  IntRect _tvrect;
  Clock _tvclock;
  bool _on, _play;

  void drawScene(const Scene&, const Vector2f&, bool);
  void drawTransition(float, const Scene&);
  void centerText(const Vector2f&);
  void animateTVFog();
  void consoleON();
};
