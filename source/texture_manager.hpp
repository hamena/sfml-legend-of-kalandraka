#ifndef TEXTURE_MANAGER_HPP
#define TEXTURE_MANAGER_HPP

#include <SFML/Graphics.hpp>
#include <vector>

using std::vector;
using namespace sf;

class TextureManager{
public:
  const Texture& loadTexture(const char* file){
    Texture *t = new Texture();
    t->loadFromFile(file);
    _textures.push_back(t);
    return *_textures.back();
  }
  ~TextureManager(){
    for (size_t i=0; i<_textures.size(); ++i)
      delete _textures[i];
  }
private:
  vector<Texture*> _textures;
};

#endif // SPRITE_MANAGER_HPP
