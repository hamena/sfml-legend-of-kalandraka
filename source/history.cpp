#include "history.hpp"

History::History(RenderWindow& w, TextureManager& tm) : 
  _window(w), _tman(tm), _tvrect(0,0,76,64), _on(false), _play(true)
{  
  if (!_font.loadFromFile("assets/fonts/ONESIZE_.TTF"))
    std::cout << " Error loading font " << std::endl;
  
  _tvscreen.setTexture(tm.loadTexture("assets/images/tv_fog.png"));
  _tvscreen.setTextureRect(_tvrect);
  _tvscreen.setPosition(99,174);
  _tvscreen.setScale(3,3);

  _txt.setFont(_font);
  _txt.setCharacterSize(20);
  _txt.setColor(Color(108,171,70));
  _escape.setFont(_font);
  _escape.setCharacterSize(20);
  _escape.setColor(Color(108,171,70));
  _escape.setPosition(0,0);
  _escape.setString("Press ESCAPE for skip intro");
  
  vector<Sentence> vaux;
  vaux.push_back(Sentence("This is the time for the big men.",1));
  vaux.push_back(Sentence("That's a required duty\nbut an undesired challenge.",0.8));
  vaux.push_back(Sentence("Only the braves can reach the top...",0.8));
  vaux.push_back(Sentence("...and finish this task:",0.8));
  vaux.push_back(Sentence("cleaning the attic...",1.5));
  _scenes.push_back(Scene(Sprite(),vaux)); vaux.clear();

  Sprite saux(tm.loadTexture("assets/images/attic_screen.png"));
  saux.setScale(3,3);
  vaux.push_back(Sentence("Check all those\nold clothes...",0.8));
  vaux.push_back(Sentence("...dusty spiderwebs...",0.8));
  vaux.push_back(Sentence("...old-fashioned\ncostumes...",0.8));
  vaux.push_back(Sentence("...christmas stuff...",0.8));
  vaux.push_back(Sentence("...hhmmmmmm...",1.2));
  vaux.push_back(Sentence("A remarkable chest...",0.8));
  vaux.push_back(Sentence("...with a treasure\ninside!",0.6));
  _scenes.push_back(Scene(saux,vaux)); vaux.clear();

  saux.setTexture(tm.loadTexture("assets/images/console_screen.png"));
  saux.setScale(3,3);
  vaux.push_back(Sentence("Awesome!",0.8));
  vaux.push_back(Sentence("My dad's videogames console!",0.6));
  vaux.push_back(Sentence("He was a true legend\nplaying arcade videogames.",0.6));
  vaux.push_back(Sentence("There's the controller...",0.6));
  vaux.push_back(Sentence("...definitely I should try this.",0.6));
  _scenes.push_back(Scene(saux,vaux)); vaux.clear();
  
  saux.setTexture(tm.loadTexture("assets/images/tv_screen.png"));
  saux.setScale(3,3);
  vaux.push_back(Sentence("I hope I can make it work.",1.2));
  vaux.push_back(Sentence("Bzzzzzz...",2));
  vaux.push_back(Sentence("It works!!",0.2));
  vaux.push_back(Sentence("Wow, a rustic scoretable.",0.6));
  vaux.push_back(Sentence("Impressive!",0.9));
  vaux.push_back(Sentence("My father tried really hard \nto beat KALANDRAKA.",0.6));
  vaux.push_back(Sentence("I think it's time to revenge my dad...", 0.6));
  vaux.push_back(Sentence("...and definitely terminate with this.",0.6));
  vaux.push_back(Sentence("Let's go!",1.2));
  _scenes.push_back(Scene(saux,vaux));
}

void History::play(){
  _txt.setString("Press any key to start");
  centerText(Vector2f(240,216));
  _window.clear();
  _window.draw(_txt);
  _window.display();

  Vector2f pos1(240,40), pos2(340,320);

  // Scene 0
  if (_play)
    drawScene(_scenes[0],pos1,false);
  // Transition
  if (_play)
  drawTransition(3,_scenes[1]);
  // Scene 1
  if (_play)
    drawScene(_scenes[1],pos2,false);
  // Scene 2
  if (_play)
    drawScene(_scenes[2],pos1,false);
  // Scene 3
  if (_play)
    drawScene(_scenes[3],pos1,true);
}

void History::drawScene(const Scene& s, const Vector2f& textpos, bool tv){
  Event event;
  size_t scenesize = s._text.size();
  unsigned count = 0;
  auto& p = s._text;
  bool wait = true;
  Clock auxclock;

  do {
    while (_window.pollEvent(event))
      if (event.type == Event::KeyPressed){
	_txt.setString("");
	wait = false;
	auxclock.restart();
	if (event.key.code == Keyboard::Escape)
	  _play = false;
      }

    if (!wait && auxclock.getElapsedTime().asSeconds() > p[count]._duration){
      wait = true;
      _txt.setString(p[count++]._str);
      centerText(textpos);
    }
    _window.clear();
    _window.draw(s._backgrd);
    _window.draw(_txt);
    _window.draw(_escape);
    if (tv){
      if (count == 2 && wait)
	consoleON();
      animateTVFog();
      _window.draw(_tvscreen);
    }
    _window.display();
  }while (count < scenesize && _play);
  sleep(seconds(2));
  _txt.setString("");
}

void History::drawTransition(float t, const Scene& s){
  RectangleShape spt(Vector2f(480,432)); spt.setFillColor(Color::Black);
  float speed = 480/t;
  Clock clock, aux;
  Vector2f pos = spt.getPosition();
  while (clock.getElapsedTime().asSeconds() < t){
    pos = spt.getPosition();
    spt.setPosition(pos.x - aux.restart().asSeconds()*speed,pos.y);
    _window.clear();
    _window.draw(s._backgrd);
    _window.draw(spt);
    _window.display();
  }
}

void History::centerText(const Vector2f& v){
  FloatRect textrect = _txt.getLocalBounds();
  _txt.setOrigin(textrect.left + textrect.width/2.0,
		 textrect.top + textrect.height/2.0);
  _txt.setPosition(Vector2f(v.x,v.y));
}

void History::animateTVFog(){
  if (200 < _tvclock.getElapsedTime().asMilliseconds()){
    _tvclock.restart();
    _tvrect.left += 76;
    if (_tvrect.left >= 228) _tvrect.left = 0;
    
    _tvscreen.setTextureRect(_tvrect);
    _tvscreen.setScale(3,3);
  }
}

void History::consoleON(){
  if (!_on){
    _tvrect.top += 64;
    _tvscreen.setTextureRect(_tvrect);
    _tvscreen.setScale(3,3);
    _on = true;
  }
}
