#include "background.hpp"

GameBackground::GameBackground(TextureManager& tm, const Font& f) : 
  _playersrect(0,0,100,115), _sntdraw(false)
{
  srand(time(0));
  _backgrd.setTexture(tm.loadTexture("assets/images/game_background.png"));
  _backgrd.setScale(3,3);
  
  _player.setTexture(tm.loadTexture("assets/images/game_players.png"));
  _player.setTextureRect(_playersrect);
  _player.setScale(3,3);
  _player.setPosition(20*3,19*3);
  _playerstime = (rand() % 80) / 10;

  _snttxt.setFont(f);
  _snttxt.setCharacterSize(20);
  _snttxt.setColor(Color(108,171,70));

  _snts.push_back("Those ghosts are\ngetting me nervious.");
  _snts.push_back("How many hits\nneeds that brick?");
  _snts.push_back("Impredecible\nbarrels...");
  _snts.push_back("That ball had\nponged quite weird.");
  _snts.push_back("I need urgently\na stick!");
  _snts.push_back("Where's the blue key!?");
  _snts.push_back("Little balls are\nannoying as hell.");
  _snts.push_back("How many marbles\ncan this snake eat?");
  _snts.push_back("Jump!!");
  _snts.push_back("There are not online?");
  _snttime = (rand() & 150) / 10;
}

void GameBackground::animate(){
  if (_sntdraw && 3 < _sntclock.getElapsedTime().asSeconds()){
    _sntclock.restart();
    _sntdraw = false;
  }
  
  if (!_sntdraw && _snttime < _sntclock.getElapsedTime().asSeconds()){
    _sntdraw = true;
    _snttxt.setString(_snts[rand()%_snts.size()]);
    centerText(Vector2f(320,60));
    _snttime = ((rand() % 130) / 10) + 2.0;
    _sntclock.restart();
  }

  if (_playerstime < _playersclock.getElapsedTime().asSeconds()){
    _playersrect.left = (_playersrect.left + 100) % 300;
    _player.setTextureRect(_playersrect);
    _player.setScale(3,3);
    _playerstime = ((rand() % 40) / 10) + 1.5;
    _playersclock.restart();
  }

  if (100 < _padclock.getElapsedTime().asMilliseconds()){
    _playersrect.top = (_playersrect.top + 115) % 230;
    _player.setTextureRect(_playersrect);
    _player.setScale(3,3);
    _padclock.restart();
  }
}

void GameBackground::draw(RenderWindow& w){
  w.draw(_backgrd);
  w.draw(_player);
  if (_sntdraw)
    w.draw(_snttxt);
}

void GameBackground::centerText(const Vector2f& v){
  FloatRect textrect = _snttxt.getLocalBounds();
  _snttxt.setOrigin(textrect.left + textrect.width/2.0,
		 textrect.top + textrect.height/2.0);
  _snttxt.setPosition(Vector2f(v.x,v.y));
}
