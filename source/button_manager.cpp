#include "button_manager.hpp"

ButtonManager::ButtonManager(TextureManager& tm, const Font& f) : 
  _buttonrect(54,27,27,27), _left(false), _right(false), _animation(false),
  _leftlock(false), _rightlock(false), _score(0)
{
  srand(time(0));
  _frame.setTexture(tm.loadTexture("assets/images/game_frame.png"));
  _frame.setScale(3,3);
  _frame.setPosition(0,309);
  _framecover.setTexture(tm.loadTexture("assets/images/game_frame2.png"));
  _framecover.setScale(3,3);
  _framecover.setPosition(0,309);

  Button spt;
  spt.setTexture(tm.loadTexture("assets/images/buttons.png"));
  spt.setTextureRect(_buttonrect);
  spt.setScale(3,3);
  
  spt.setPosition(TILE*0+LX,LY);
  spt.set_type(generateLeftButton());
  spt.setTextureRect(_buttonrect);
  _lefthand.push_back(spt); 
  spt.setPosition(TILE*1+LX,LY);
  spt.set_type(generateLeftButton());
  spt.setTextureRect(_buttonrect);
  _lefthand.push_back(spt); 
  spt.setPosition(TILE*2+LX,LY);
  spt.set_type(generateLeftButton());
  spt.setTextureRect(_buttonrect);
  _lefthand.push_back(spt); 

  spt.setPosition(RX-2*TILE,RY);
  spt.set_type(generateRightButton());
  spt.setTextureRect(_buttonrect);
  _righthand.push_back(spt);
  spt.setPosition(RX-1*TILE,RY);
  spt.set_type(generateRightButton());
  spt.setTextureRect(_buttonrect);
  _righthand.push_back(spt);
  spt.setPosition(RX-0*TILE,RY);
  spt.set_type(generateRightButton());
  spt.setTextureRect(_buttonrect);
  _righthand.push_back(spt);

  _scoretxt.setFont(f);
  _scoretxt.setCharacterSize(30);
  _scoretxt.setPosition(350,20);
  _scoretxt.setColor(Color(108,171,70));
}

void ButtonManager::checkLeftHand(unsigned type){
  if (_left && !_leftlock){
    if (_lefthand.back().check(type)){
      _score += 10;
      _scoretxt.setString(to_string(_score));
    }
    _leftlock = true;
  }else if (_lefthand.back().check(NONE) && !_leftlock){
    _leftlock = _left = true;
  }
}

void ButtonManager::checkRightHand(unsigned type){
  if (_right && !_rightlock){
    if (_righthand.back().check(type)){
      _score += 10;
      _scoretxt.setString(to_string(_score));
    }
    _rightlock = true;
  }else if (_righthand.back().check(NONE) && !_rightlock){
    _rightlock = _right = true;
  }
}

void ButtonManager::move(){
  if (_left && _right){
    if (!_animation){ 
      _animation = true; 
      _c1.restart();
      rotateLeft();
      rotateRight();
      _reaction.restart();
    }else{ }
  }else{
    _lefthand[0].setPosition(TILE*0+LX,LY);
    _lefthand[1].setPosition(TILE*1+LX,LY);
    _lefthand[2].setPosition(TILE*2+LX,LY);
    
    _righthand[0].setPosition(RX-0*TILE,RY);
    _righthand[1].setPosition(RX-1*TILE,RY);
    _righthand[2].setPosition(RX-2*TILE,RY);
    _c2.restart();
  }
}

void ButtonManager::deactivate(unsigned& lh, unsigned& rh){
  if (_animation && 0.25 < _c1.getElapsedTime().asSeconds()){
    lh = rh = _right = _left = false;
    _rightlock = _leftlock = _animation = false;
    _reaction.restart();
  }
}

void ButtonManager::reaction(unsigned& lh, unsigned& rh){
  if (1.5 < _reaction.getElapsedTime().asSeconds()){
    lh = rh =_right = _left = false;
    _rightlock = _leftlock = _animation = false;
    ++_count;
    _c1.restart();
    rotateLeft();
    rotateRight();
    _reaction.restart();
  }
}

void ButtonManager::draw(RenderWindow& w) const{
  w.draw(_frame);
  w.draw(_scoretxt);
  for (unsigned i=0; i<3; ++i){
    w.draw(_lefthand[i]);
    w.draw(_righthand[i]);
  }
  w.draw(_framecover);
}

void ButtonManager::rotateLeft(){
  _lefthand[2] = _lefthand[1];
  _lefthand[1] = _lefthand[0];
  _lefthand[0].set_type(generateLeftButton());
  _lefthand[0].setTextureRect(_buttonrect);
  _lefthand[0].setScale(3,3);
  _lefthand[0].setPosition(_lefthand[1].getPosition().x-TILE,LY);
}

void ButtonManager::rotateRight(){
  _righthand[2] = _righthand[1];
  _righthand[1] = _righthand[0];
  _righthand[0].set_type(generateRightButton());
  _righthand[0].setTextureRect(_buttonrect);
  _righthand[0].setScale(3,3);
  _righthand[0].setPosition(_righthand[1].getPosition().x+TILE,LY);
}

unsigned ButtonManager::generateLeftButton(){
  switch (rand() % 5){
  case UP:
    _buttonrect.left = 0;
    _buttonrect.top = 0;
    return UP;
    break;
  case LEFT:
    _buttonrect.left = 81;
    _buttonrect.top = 0;
    return LEFT;
    break;
  case DOWN:
    _buttonrect.left = 54;
    _buttonrect.top = 0;
    return DOWN;
    break;
  case RIGHT:
    _buttonrect.left = 27;
    _buttonrect.top = 0;
    return RIGHT;
    break;
  default:
    _buttonrect.left = 54;
    _buttonrect.top = 27;
    return NONE;
    break;
  }
}

unsigned ButtonManager::generateRightButton(){
  switch ((rand() % 3)+5){
  case A:
    _buttonrect.left = 0;
    _buttonrect.top = 27;
    return A;
    break;
  case B:
    _buttonrect.left = 27;
    _buttonrect.top = 27;
    return B;
    break;
  default:
    _buttonrect.left = 54;
    _buttonrect.top = 27;
    return NONE;
    break;
  }
}
