#include <iostream>
#include <SFML/Graphics.hpp>
#include "texture_manager.hpp"
#include "history.hpp"
#include "background.hpp"
#include "button_manager.hpp"
#include "caption.hpp"

#define SCALE 3
#define SCRN_W 160*SCALE
#define SCRN_H 144*SCALE

using namespace sf;

int main(){
  RenderWindow window(sf::VideoMode(SCRN_W, SCRN_H), "Legend Of Kalandraka");
  window.setFramerateLimit(60);
  window.setKeyRepeatEnabled(false);
  TextureManager tm;
  History* h = new History(window,tm);
  h->play();
  delete h;

  Font font;
  if (!font.loadFromFile("assets/fonts/ONESIZE_.TTF"))
    std::cout << "Error loading font " << std::endl;
  GameBackground bg(tm,font);
  ButtonManager bm(tm,font);
  CaptionManager capture(bm);
  
  while (window.isOpen()){
    Event event;
    while (window.pollEvent(event)){
      if (event.type == Event::Closed)
	window.close();
    }

    capture.captureLeftHand();
    capture.captureRightHand();
    capture.respond();

    if (bm.game_over()) break;

    bg.animate();
    window.clear();
    bg.draw(window);
    bm.draw(window);
    window.display();
  }
  return 0;
}
