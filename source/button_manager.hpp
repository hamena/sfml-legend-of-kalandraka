#ifndef BUTTON_MANAGER_HPP
#define BUTTON_MANAGER_HPP

#include <SFML/Graphics.hpp>
#include <vector>
#include <string>
#include <ctime>
#include <cstdlib>
#include "texture_manager.hpp"

#define LX -4*3
#define LY 110*3
#define RX 138*3
#define RY 110*3
#define TILE 27*3

using std::vector;
using std::to_string;
using namespace sf;

enum Buttons{NONE, UP, LEFT, DOWN, RIGHT, A, B};

class Button : public Sprite{
public:
  Button() : Sprite(), _type(NONE) {}
  bool check(unsigned t) const { return t == _type; }
  void set_type(unsigned t) { _type = t; }
  unsigned get_type() const{ return _type; }
private:
  unsigned _type;
};

class ButtonManager{
public:
  ButtonManager(TextureManager&, const Font&);
  void checkLeftHand(unsigned type);
  void checkRightHand(unsigned type);
  void move();
  void draw(RenderWindow&) const;
  void lightLeft(){ _left = true; }
  void lightRight(){ _right = true; }
  void deactivate(unsigned&, unsigned&);
  void reaction(unsigned&, unsigned&);
  bool game_over() const{ return _count >= 1000; }
private:
  vector<Button> _lefthand;
  vector<Button> _righthand;
  IntRect _buttonrect;
  bool _left, _right;
  bool _animation, _leftlock, _rightlock;
  Clock _c1, _c2, _reaction;
  Sprite _frame, _framecover;
  unsigned _count;

  unsigned _score;
  Text _scoretxt;

  void rotateLeft();
  void rotateRight();
  unsigned generateLeftButton();
  unsigned generateRightButton();
};

#endif // BUTTON_MANAGER_HPP
