#include "caption.hpp"

void CaptionManager::captureLeftHand(){
  if (!_lefthand){
    if (Keyboard::isKeyPressed(Keyboard::W)){
      _lefthand = UP; _bmanager.lightLeft();
    }else if (Keyboard::isKeyPressed(Keyboard::S)){
      _lefthand = DOWN; _bmanager.lightLeft();
    }else if (Keyboard::isKeyPressed(Keyboard::A)){
      _lefthand = LEFT; _bmanager.lightLeft();
    }else if (Keyboard::isKeyPressed(Keyboard::D)){
      _lefthand = RIGHT; _bmanager.lightLeft();
    }
  }
}

void CaptionManager::captureRightHand(){
  if (!_righthand){
    if (Keyboard::isKeyPressed(Keyboard::K)){
      _righthand = A; _bmanager.lightRight();
    }else if (Keyboard::isKeyPressed(Keyboard::M)){
      _righthand = B; _bmanager.lightRight();
    }
  }
}

void CaptionManager::respond(){
  _bmanager.reaction(_lefthand, _righthand);
  _bmanager.checkLeftHand(_lefthand);
  _bmanager.checkRightHand(_righthand);
  _bmanager.move();
  _bmanager.deactivate(_lefthand, _righthand);
}
