#ifndef BACKGROUND_HPP
#define BACKGROUND_HPP

#include <SFML/Graphics.hpp>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <string>
#include "texture_manager.hpp"

using std::string;
using std::vector;
using namespace sf;

class GameBackground{
public:
  GameBackground( TextureManager&, const Font&);
  void animate();
  void draw(RenderWindow&);
private:
  Sprite _backgrd;

  Sprite _player;
  IntRect _playersrect;
  Clock _playersclock, _padclock;
  float _playerstime;

  Text _snttxt;
  vector<string> _snts;
  Clock _sntclock;
  float _snttime;
  bool _sntdraw;

  void centerText(const Vector2f&);
};

#endif // BACKGROUND_HPP
