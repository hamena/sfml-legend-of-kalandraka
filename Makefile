CXX = g++
CXXFLAGS = -std=c++11 -Wall -O2
OBJECTS = source/main.o source/history.o source/background.o source/button_manager.o source/caption.o
EXEC = main

$(EXEC): ${OBJECTS}
	g++ -o $@ $^ -lsfml-graphics -lsfml-window -lsfml-system

run:
	./$(EXEC)

clean:
	rm -f $(EXEC) ${OBJECTS} *~ *.o
